package br.mg.gov.main;

import java.awt.List;

import br.mg.gov.entidade.Aluno;

public class ImprimirSolicitacao {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		private List<Aluno> lista = new ArrayList<Aluno>();

		Aluno a1 = new Aluno("Ana K1", "s1234343", 10.0f, 0);
		Aluno a2 = new Aluno("Ana K2", "m1234343", 8.0f, 0);
		Aluno a3 = new Aluno("Ana K3", "h1234343", 7.0f, 0);
		Aluno a4 = new Aluno("Ana K4", "k1234343", 5.0f, 10);
		Aluno a5 = new Aluno("Ana K5", "t1234343", 7.5f, 4);
		Aluno a6 = new Aluno("Ana K6", "e1234343", 4.0f, 3);
		Aluno a7 = new Aluno("Ana K7", "ff1234343", 3.0f, 20);
		Aluno a8 = new Aluno("Ana K8", "d1234343", 8.40f, 0);
		Aluno a9 = new Aluno("Ana K9", "b1234343", 4.60f, 0);
		Aluno a10 = new Aluno("Ana K10", "a1234343", 9.0f, 0);


	}

}
