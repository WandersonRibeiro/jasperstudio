package br.mg.gov.entidade;

public class Aluno {

	private String nome;
	private String matricula;
	private Float nota;
	private Integer faltas;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Float getNota() {
		return nota;
	}
	public void setNota(Float nota) {
		this.nota = nota;
	}
	public Integer getFaltas() {
		return faltas;
	}
	public void setFaltas(Integer faltas) {
		this.faltas = faltas;
	}


	public Aluno(String nome, String matricula, Float f, Integer faltas) {
		super();
		this.nome = nome;
		this.matricula = matricula;
		this.nota = f;
		this.faltas = faltas;
	}

	@Override
	public String toString() {
		return "Aluno [nome=" + nome + ", matricula=" + matricula + ", nota=" + nota + ", faltas=" + faltas + "]";
	}


}
